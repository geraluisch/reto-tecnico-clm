1- Endpoint buscador de películas:
http://localhost:1987/findMovieByTitle/:title

* Header para consultar con el año 'year' : yyyy

2- Endpoint obtener todas las películas:
http://localhost:1987/findAll

* Header de paginación 'page' : n

3- Endpoint buscar y reemplazar
http://localhost:1987/findAndReplace

* Request body {"movie":"", "find":"", "replace":""}
