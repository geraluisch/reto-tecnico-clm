const koa = require('koa');
const respond = require('koa-respond');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const logger = require("koa-logger");

const movieRoute = require("./modules/movies/movies.router");

const db = require('./integrations/mongodb');
db.connect();


const app = new koa();

app    
    .use(bodyParser())
    .use(respond())
    .use(movieRoute)    
    .use(cors())
    .listen(1987, () => {
        console.log('Koa server is working');
    });