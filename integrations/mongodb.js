const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

module.exports = class DB {
    
    static connect(){
        return mongoose.connect('mongodb+srv://luig:Lg123456*@cluster0.fhunu.mongodb.net/moviesdb?retryWrites=true&w=majority', {
            promiseLibrary : global.Promise,
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then((db) => console.log("db is connected"))
        .catch((err) => console.log(err));
    }
}