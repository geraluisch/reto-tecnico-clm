const MovieModel = require('./movies.model');
const axios = require('axios');

const movieObj = { 
    Title: '',
    Year: '',
    Released: '',    
    Genre: '',
    Director: '',
    Actors: '',
    Plot: '',
    Ratings: []
}

const saveMovie = ( data ) => {

    const { 
        Title,
        Year,
        Released,    
        Genre,
        Director,
        Actors,
        Plot,
        Ratings
    } = data;

    const movie = Object.create(movieObj);
    let movieJSON = [];
    movie.Title = Title;
    movie.Year = Year;
    movie.Released = Released;
    movie.Genre = Genre;
    movie.Director = Director;
    movie.Actors = Actors;
    movie.Plot = Plot;
    movie.Ratings = Ratings;

    movieJSON.push(movie);

    MovieModel.create(movie);

    return movie;

}

exports.helloController = async (ctx, next) => {
    ctx.body = "Hello from koa";
    await next();
}

exports.findAndCreateByTitleController = async (ctx, next) => {
    
    let notFound = true;
    let movieYear = '';
    let query = {};
    
    query['Title'] = { $regex : new RegExp(`^${ctx.params.title.replace('-',' ')}`), $options: 'i' };  
    
    if( ctx.headers['year'] !== undefined ) {
        query['Year'] = ctx.headers['year'];
        movieYear = `&y=${ctx.headers['year']}`;
    }
    
    await MovieModel.find(query).then((res) => {        
        notFound = res.length <= 0;
        ctx.body = JSON.stringify(res);
        ctx.status = 200;
    }).catch((err) => {
        ctx.status = 500; 
        ctx.body = err;
    });
    
    if( notFound && ctx.status != 500) {

        const {data, status} = await axios.get(
            `http://www.omdbapi.com/?t=${ctx.params.title}${movieYear}&apikey=c00b0e7c`,
        );
          
        if( status === 200 && data['Response'] === "True") {            
            
            ctx.body = JSON.stringify(saveMovie( data ));
            ctx.status = 200;
        }
        else {
            ctx.body = JSON.parse('{ "response": false, "message": "Movie not found"}');
            ctx.status = 404;                    
        }    
    }
            

    await next();
}

exports.findAllController = async (ctx, next) => {
    
    const resultsPerPage = 5;
    let page = 0;
    
    if( ctx.headers['page'] !== undefined ) 
        page = ctx.headers['page'] - 1;

    await MovieModel.find()
        .limit(resultsPerPage)
        .skip(resultsPerPage * page)
        .then((res) => {        
            if( res.length <= 0 ) {
                ctx.body = JSON.parse('{ "response": false, "message": "Empty database - Movies not found"}');
                ctx.status = 404; 
            } else {
                ctx.body = JSON.stringify(res);
                ctx.status = 200;
            }
        }).catch((err) => {
            ctx.status = 500; 
            ctx.body = err;
        });   

    await next();
}



exports.findAndReplaceController = async (ctx, next) => {
    
    let movie = undefined;
    let sFind = undefined;
    let sReplace = undefined;

    if (Object.keys(ctx.request.body || {}).length) {
        movie = ctx.request.body['movie'];
        sFind = ctx.request.body['find'];
        sReplace = ctx.request.body['replace'];                
    }
    
    if( movie === undefined || sFind === undefined || sReplace === undefined ) {
        ctx.status = 400;
        ctx.body = JSON.parse('{ "response": false, "message": "invalid request body"}');
    } else {
       let query = {};
       query['Title'] = { $regex : new RegExp(`^${movie.replace('-',' ')}`), $options: 'i' };

       await MovieModel.find(query)        
        .then((res) => {        
            if( res.length <= 0 ) {
                ctx.body = JSON.parse('{ "response": false, "message": "Movie not found"}');
                ctx.status = 404; 
            } else {                
                try {
                    const regex = new RegExp(sFind, 'gi');
                    const plot = res[0]['Plot'];

                    ctx.body = plot.replace(regex, sReplace);
                    ctx.status = 200;                    
                } catch (err) {
                    ctx.status = 500; 
                    ctx.body = err;
                }
            }
        }).catch((err) => {
            ctx.status = 500; 
            ctx.body = err;
        }); 
        
    }
    
    await next();
}