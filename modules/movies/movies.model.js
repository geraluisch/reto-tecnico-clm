const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const movieSchema = new Schema({
    Title: {
        type: String,
        required: true,
    },
    Year: {
        type: String,
        required: true,
    },
    Released: {
        type: String,
        required: true,
    },    
    Genre: {
        type: String,
        required: true,
    },
    Director: {
        type: String,
        required: true,
    },
    Actors: {
        type: String,
        required: true,
    },
    Plot: {
        type: String,
        required: true,
    },
    Ratings:{
        type: [{
            Source:{
                type: String,
                required: true,
            }, 
            Value:{
                type: String,
                required: true,
            },
        }],
        required: true,
    }    
})

const Movies = mongoose.model('movies', movieSchema);

module.exports = Movies;