const { helloController, 
        findAndCreateByTitleController, 
        findAllController,
        findAndReplaceController } = require("./movies.controller");
const Router = require("koa-router");
const router = new Router();

router.get('/hello', helloController);
router.get('/findMovieByTitle/:title', findAndCreateByTitleController);
router.get('/findAll', findAllController);
router.post('/findAndReplace',findAndReplaceController);

module.exports = router.routes();